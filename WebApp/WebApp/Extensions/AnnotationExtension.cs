﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Extensions
{
    public class YearRangeTillDateAttribute:RangeAttribute
    {
        public YearRangeTillDateAttribute(int StatrYear):base(StatrYear,DateTime.Today.Year)
        {

        }
    }
}
