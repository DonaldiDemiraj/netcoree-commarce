﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.AppDbContext;
using WebApp.Helpers;
using WebApp.Models;

namespace WebApp.Data
{
    public class DBInitialize : IDBInitializer
    {
        private readonly VroomDbContext _db;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public DBInitialize(VroomDbContext db, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _db = db;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public async void Initialize()
        {
            //Add pending mgration if exists
            if(_db.Database.GetAppliedMigrations().Count() > 0)
            {
                _db.Database.Migrate();
            }

            //Exit if role alerady exists
            if (_db.Roles.Any(r => r.Name == Helpers.Roles.Admin)) return;

            //Create Admin Role
            _roleManager.CreateAsync(new IdentityRole(Roles.Admin)).GetAwaiter().GetResult();

            //Create Admin User
            _userManager.CreateAsync(new ApplicationUser
            {
                UserName = "Admin",
                Email = "Admin@gmail.com",
                EmailConfirmed = true,
            },"Admin123").GetAwaiter().GetResult();

            //Assign role To Admin User
            await _userManager.AddToRoleAsync(await _userManager.FindByNameAsync("Admin"), Roles.Admin);
        }
    }
}
