﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.AppDbContext;
using WebApp.Models;
using WebApp.Models.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using cloudscribe.Pagination.Models;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TechController : Controller
    {
        private readonly VroomDbContext _db;
        private readonly IHostingEnvironment _ihostingEnvironment;

        [BindProperty]
        public TechViewModel TechVM { get; set; }

        public TechController(VroomDbContext db, IHostingEnvironment ihostingEnvironment)
        {
            _db = db;
            _ihostingEnvironment = ihostingEnvironment; 
            TechVM = new TechViewModel()
            {
                Makes = _db.Makes.ToList(),
                Models = _db.Models.ToList(),
                Tech = new Models.Tech()
            };
        }

        [AllowAnonymous]
        public IActionResult Index(string searchString, string sortOrder, int pageNumber=1, int pageSize=2)
        {
            ViewBag.CurrentSortOrder = sortOrder;
            ViewBag.CurrentFilter = searchString;
            ViewBag.PriceSortParam = String.IsNullOrEmpty(sortOrder) ? "price_desc" : "";
            int ExcludeRecord = (pageSize * pageNumber) - pageSize;

            var Techs = from b in _db.Teches.Include(m => m.Make).Include(m => m.Model)
                        select b;

            var TechCount = Techs.Count();

            if (!String.IsNullOrEmpty(searchString))
            {
                Techs = Techs.Where(b => b.Make.Name.Contains(searchString));
                TechCount = Techs.Count();
            }

            //Sorting Logic
            switch(sortOrder)
            {
                case "price_desc":
                    Techs = Techs.OrderByDescending(b => b.Price);
                    break;
                default:
                    Techs = Techs.OrderBy(b => b.Price);
                    break;
            }


            Techs = Techs
                .Skip(ExcludeRecord)
                .Take(pageSize);

            var result = new PagedResult<Tech>
            {
                Data = Techs.AsNoTracking().ToList(),
                TotalItems = TechCount,
                PageNumber = pageNumber,
                PageSize = pageSize,
            };
            return View(result);
        }

        //GET MEthod
        public IActionResult Create()
        {
            return View(TechVM);
        }

        //Post
        [HttpPost, ActionName("Create")]
        //[ValidateAntiForgeryToken]
        public IActionResult CreatePost()
        {
            if (!ModelState.IsValid)
            {
                TechVM.Makes = _db.Makes.ToList();
                TechVM.Models = _db.Models.ToList();
                return View(TechVM);
            }
            _db.Teches.Add(TechVM.Tech);
            _db.SaveChanges();
            UploadImageIfAvailable();
            TempData["save"] = "Tech type have been saved";
            return RedirectToAction(nameof(Index));
        }

        private void UploadImageIfAvailable()
        {

            //Get Tech id we have in database
            var TechID = TechVM.Tech.Id;

            //Get root path to save the file on the server 
            string wwrootPath = _ihostingEnvironment.WebRootPath;

            //Get the uploaded files
            var files = HttpContext.Request.Form.Files;

            //Get the references of DBSet in database
            var SaveTech = _db.Teches.Find(TechID);

            //Upload the files on server and save the path image
            if (files.Count != 0)
            {
                var ImgPath = @"images\tech";
                var Extension = Path.GetExtension(files[0].FileName);
                var RelativeImgPath = ImgPath + TechID + Extension;
                var AbsImgPath = Path.Combine(wwrootPath, RelativeImgPath);

                //Upload the file on the server
                using (var fileStream = new FileStream(AbsImgPath, FileMode.Create))
                {
                    files[0].CopyTo(fileStream);
                }

                //set the path on database
                SaveTech.ImagePath = RelativeImgPath; _db.SaveChanges();
            }
        }

        
        public IActionResult Edit(int id)
        {
            TechVM.Tech = _db.Teches.Include(m => m.Make).SingleOrDefault(m => m.Id == id);

            //Filter the models 
            TechVM.Models = _db.Models.Where(m => m.MakeFK == TechVM.Tech.ModelFK);

            if (TechVM.Tech == null)
            {
                return NotFound();
            }
            return View(TechVM);
        }   

        [HttpPost, ActionName("Edit")]
        public IActionResult EditPost()
        {
            if (!ModelState.IsValid)
            {
                TechVM.Makes = _db.Makes.ToList();
                TechVM.Models = _db.Models.ToList();
                return View(TechVM);
            }
            _db.Teches.Update(TechVM.Tech);
            UploadImageIfAvailable();
            _db.SaveChanges();
            TempData["update"] = "Product type have been update";
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            Tech tech = _db.Teches.Find(id);
            if (tech == null)
            {
                return NotFound();
            }
            _db.Teches.Remove(tech);
            _db.SaveChanges();
            TempData["delet"] = "Product type have been deleted";
            return RedirectToAction(nameof(Index));
        }

        [AllowAnonymous]
        public IActionResult View(int id)
        {
            TechVM.Tech = _db.Teches.Include(m => m.Make).SingleOrDefault(m => m.Id == id);

            if (TechVM.Tech == null)
            {
                return NotFound();
            }
            return View(TechVM);
        }
    }
}
