﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Extensions;

namespace WebApp.Models
{
    public class Tech
    {
        public int Id { get; set; }

        public Make Make { get; set; }
        [RegularExpression("^[1-9]*$",ErrorMessage ="Select Make")]
        public int MakeFK { get; set; }

        public Model Model  { get; set; }
        [RegularExpression("^[1-9]*$", ErrorMessage = "Select Model")]
        public int ModelFK { get; set; }

        [Required(ErrorMessage = "Provide Year")]
        [YearRangeTillDate(2010,ErrorMessage ="Invalid Year")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Provide Mileage")]
        [Range(1,int.MaxValue,ErrorMessage ="Provide Mileage")]
        public int Mileage { get; set; }
        
        [Required]
        public int Features { get; set; }

        [Required(ErrorMessage ="Provide Seller Name")]
        public string SellerName { get; set; }
        
        [EmailAddress(ErrorMessage ="Invalid Email ID")]
        public string SellerEmail { get; set; }

        [Required(ErrorMessage = "Provide Seller Phone")]
        public string SellerPhone { get; set; }

        [Required(ErrorMessage = "Provide Selling  Price")]
        public string Price { get; set; }
        
        [Required]
        [RegularExpression("^[A-Z]*$", ErrorMessage = "Select Currency")]
        public string Currency { get; set; }

        public string ImagePath { get; set; }
    }
}
